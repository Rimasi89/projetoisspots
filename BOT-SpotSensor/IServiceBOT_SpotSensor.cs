﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;

namespace BOT_SpotSensor
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IServiceBOT_SpotSensor
    {
        [OperationContract]
        string CreateSensorData(string nomeParque);
        [OperationContract]
        string CreateSingleSensorData(string nomeParque);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class ParkingSpot
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public bool SlotStatus { get; set; }
        [DataMember]
        public string Timestamp { get; set; }
        [DataMember]
        public bool BatteryStatus { get; set; }
    }
}
