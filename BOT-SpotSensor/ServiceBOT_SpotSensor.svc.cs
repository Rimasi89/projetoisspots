﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Xml;

namespace BOT_SpotSensor
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ServiceBOT_SpotSensor : IServiceBOT_SpotSensor
    {
        string filepath = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"App_Data\configs.xml";

        public string CreateSensorData(string nomeParque)
        {

            String returnXML = String.Empty;
            XmlDocument spots = new XmlDocument();
            XmlDocument config = new XmlDocument();
            config.Load(filepath);


            //(1) the xml declaration is recommended, but not mandatory
            XmlDeclaration xmlDeclaration = spots.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = spots.DocumentElement;
            spots.InsertBefore(xmlDeclaration, root);
            returnXML += spots.OuterXml;
            //(2) string.Empty makes cleaner code
            XmlNode configPark = config.SelectSingleNode($"parkingLot[id=\"{nomeParque}\"]");
            XmlNode configParkNumberofSpots = configPark.SelectSingleNode("numberOfSpots");

            int.TryParse(configParkNumberofSpots.InnerText, out int nrLugares);
            for (int i = 0; i < nrLugares; i++)
            {
                returnXML += createReturnXML(nomeParque, i.ToString());
            }

            return returnXML;
        }

        public string CreateSingleSensorData(string nomeParque)
        {

            String returnXML = String.Empty;
            XmlDocument spots = new XmlDocument();
            XmlDocument config = new XmlDocument();
            config.Load(filepath);


            //(1) the xml declaration is recommended, but not mandatory
            XmlDeclaration xmlDeclaration = spots.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = spots.DocumentElement;
            spots.InsertBefore(xmlDeclaration, root);
            returnXML += spots.OuterXml;
            //(2) string.Empty makes cleaner code
            XmlNode configPark = config.SelectSingleNode($"parkingLot[id=\"{nomeParque}\"]");
            XmlNode configParkNumberofSpots = configPark.SelectSingleNode("numberOfSpots");

            int.TryParse(configParkNumberofSpots.InnerText, out int nrLugares);
            Random rnd = new Random();
            int currentRequest = rnd.Next(0, nrLugares+1);
            returnXML += createReturnXML(nomeParque, currentRequest.ToString());


            return returnXML;
        }

        private String createReturnXML(String id, String currentRequest)
        {
            String xmlReturn = String.Empty;
            XmlDocument doc = new XmlDocument();
            XmlElement park = doc.CreateElement("parkingSpot");

            XmlElement idElem = doc.CreateElement("id");
            idElem.InnerText = id;

            XmlElement typeElem = doc.CreateElement("type");
            typeElem.InnerText = "ParkingSpot";

            XmlElement nameElem = doc.CreateElement("name");
            nameElem.InnerText = "B-" + currentRequest;

            XmlElement locationElem = doc.CreateElement("location");
            locationElem.InnerText = "";

            XmlElement statusElem = doc.CreateElement("status");
            XmlElement valueElem = doc.CreateElement("value");

            Random rnd = new Random();
            int slotStatus = rnd.Next(0, 2);
            valueElem.InnerText = slotStatus == 0 ? "free" : "occupied";
            XmlElement timestampElem = doc.CreateElement("timestamp");
            timestampElem.InnerText = DateTime.Now.ToString();

            int batteryStatus = rnd.Next(0, 2);
            XmlElement batteryStatusElem = doc.CreateElement("batteryStatus");
            batteryStatusElem.InnerText = batteryStatus == 0 ? "0" : "1";

            park.AppendChild(idElem);
            park.AppendChild(typeElem);
            park.AppendChild(nameElem);
            statusElem.AppendChild(valueElem);
            statusElem.AppendChild(timestampElem);
            park.AppendChild(statusElem);
            park.AppendChild(batteryStatusElem);
            doc.AppendChild(park);

            return doc.OuterXml;
        }
    }

}
