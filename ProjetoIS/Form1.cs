﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using BOT_SpotSensor;
using Excel = Microsoft.Office.Interop.Excel;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace ParkDACE
{
    public partial class Form1 : Form
    {
        ParkingSensorNodeDll.ParkingSensorNodeDll dll = null;
        ServiceBOT_SpotSensor serviceBot = null;
        MqttClient client = null;
        string[] topics = {"spot"};

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dll = new ParkingSensorNodeDll.ParkingSensorNodeDll();
            if (!connectToMosquitto())
            {
                MessageBox.Show("Error connecting!");
                Close();
            }
            dll.Initialize(SpotsPark, timer1.Interval);
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void textRichDllInfo_TextChanged(object sender, EventArgs e)
        {
        }

        public void SpotsPark(string str)
        {
            
            this.BeginInvoke((MethodInvoker)delegate {
                textRichDllInfo.AppendText(str + "\n");
            });

            enrichAndSendDllParkingSpots(str);
            

        }

        private void enrichAndSendDllParkingSpots (String str)
        {
            String filepathGeoLocationDll = AppDomain.CurrentDomain.BaseDirectory + @"DLL\Campus_2_A_Park1.xlsx";
            string[] spotDllStr = str.Split(';');

            XmlDocument xmlToSend = new XmlDocument();
            XmlElement root = xmlToSend.CreateElement("spots");
            string parkName = spotDllStr[0];

            root.SetAttribute("parkName", parkName);
            xmlToSend.AppendChild(root);
            XmlElement spot = xmlToSend.CreateElement("spot");
            XmlElement id = xmlToSend.CreateElement("id");
            id.InnerText = spotDllStr[1].ToString();
            XmlElement timestamp = xmlToSend.CreateElement("timestamp");
            timestamp.InnerText = spotDllStr[2].ToString();
            XmlElement parkingSpotStatus = xmlToSend.CreateElement("parkingSpotStatus");
            parkingSpotStatus.InnerText = spotDllStr[3].ToString();
            XmlElement batteryStatus = xmlToSend.CreateElement("batteryStatus");
            batteryStatus.InnerText = spotDllStr[4].ToString();
            XmlElement type = xmlToSend.CreateElement("type");
            type.InnerText = "ParkingSpot";
            //adiciona a geolocation
            XmlElement geoLocation = xmlToSend.CreateElement("location");
            geoLocation.InnerText = getGeoLocation(parkName, id.InnerText, filepathGeoLocationDll);
            spot.AppendChild(id);
            spot.AppendChild(timestamp);
            spot.AppendChild(parkingSpotStatus);
            spot.AppendChild(batteryStatus);
            spot.AppendChild(geoLocation);
            spot.AppendChild(type);
            root.AppendChild(spot);

            string aenviar = xmlToSend.InnerXml;
            enviarXmlStringToMosquitto(aenviar);
        }

        private void enviarXmlStringToMosquitto(string aenviar)
        {
            byte[] msg = Encoding.UTF8.GetBytes(aenviar);
            client.Publish(topics[0], msg);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            dll.Stop();

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            serviceBot = new ServiceBOT_SpotSensor();
            String xml = serviceBot.CreateSensorData("Campus_2_B_Park2");
            richTextBox1.AppendText(xml + "\n");

        }

        private void button2_Click(object sender, EventArgs e)
        {

            serviceBot = new ServiceBOT_SpotSensor();
            String xml = serviceBot.CreateSingleSensorData("Campus_2_B_Park2");
            richTextBox1.AppendText(xml + "\n\n");
        }

        private string getGeoLocation(string parkName, string spotId, string filepath)
        {
            Excel.Application excelAplication = new Excel.Application();
            excelAplication.Visible = false;
            Excel.Workbook excelWorkbook = excelAplication.Workbooks.Open(filepath);
            Excel.Worksheet excelWorksheet = (Excel.Worksheet) excelWorkbook.ActiveSheet;
            int i;
            string parkNameExcel;
            for (i = 2; i < 10; i+=2)//meter aqui o nr total de parks
            {
                parkNameExcel = excelWorksheet.Cells[1, i].Value;
                if (!String.Equals(parkName,parkNameExcel))
                {
                    continue;
                }
                break;
            }
            string[] linhaSpotId = spotId.Split('-');
            int linhaDoExcel = 5 + int.Parse(linhaSpotId[1]);
            string content = excelWorksheet.Cells[linhaDoExcel, i].Value;

            //limpeza dos processos
            excelWorkbook.Close();
            excelAplication.Quit();
            ReleaseCOMObjects(excelWorksheet);
            ReleaseCOMObjects(excelWorkbook);
            ReleaseCOMObjects(excelAplication);

            return content;
        }

        private static void ReleaseCOMObjects(Object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }
            catch (Exception ex)
            {
                obj = null;
                System.Diagnostics.Debug.WriteLine("Exception release ...");
            }
            finally
            {
                GC.Collect();
            }
        }

        private Boolean connectToMosquitto ()
        {
            client = new MqttClient("127.0.0.1");
            client.Connect(Guid.NewGuid().ToString());
            if (!client.IsConnected)
            {
                return false;
            }
            return true;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            serviceBot = new ServiceBOT_SpotSensor();
            String xml = serviceBot.CreateSingleSensorData("Campus_2_B_Park2");
            richTextBox1.AppendText(xml + "\n\n");
            makeXMLToSendBot(xml);
        }

        private void makeXMLToSendBot(string xml)
        {
            XmlDocument xmlBot = new XmlDocument();
            xmlBot.LoadXml(xml);

            String filepathGeoLocationDll = AppDomain.CurrentDomain.BaseDirectory + @"DLL\Campus_2_A_Park1.xlsx";
            XmlDocument xmlToSend = new XmlDocument();
            XmlElement root = xmlToSend.CreateElement("spots");
            string parkName = xmlBot.SelectSingleNode("/parkingSpot/id").InnerText;
            root.SetAttribute("parkName", parkName);
            xmlToSend.AppendChild(root);
            XmlElement spot = xmlToSend.CreateElement("spot");
            XmlElement id = xmlToSend.CreateElement("id");
            id.InnerText = xmlBot.SelectSingleNode("/parkingSpot/name").InnerText;
            XmlElement timestamp = xmlToSend.CreateElement("timestamp");
            timestamp.InnerText = xmlBot.SelectSingleNode("/parkingSpot/status/timestamp").InnerText;
            XmlElement parkingSpotStatus = xmlToSend.CreateElement("parkingSpotStatus");
            int parkingSStatus = 1;
            if (xmlBot.SelectSingleNode("/parkingSpot/status/value").InnerText.Equals("occupied"))
            {
                parkingSStatus = 0;
            }
            parkingSpotStatus.InnerText = parkingSStatus.ToString();
            XmlElement batteryStatus = xmlToSend.CreateElement("batteryStatus");
            batteryStatus.InnerText = xmlBot.SelectSingleNode("/parkingSpot/batteryStatus").InnerText;
            XmlElement type = xmlToSend.CreateElement("type");
            type.InnerText = xmlBot.SelectSingleNode("/parkingSpot/type").InnerText; ;
            //adiciona a geolocation
            XmlElement geoLocation = xmlToSend.CreateElement("location");
            geoLocation.InnerText = getGeoLocation(parkName, id.InnerText, filepathGeoLocationDll);
            spot.AppendChild(id);
            spot.AppendChild(timestamp);
            spot.AppendChild(parkingSpotStatus);
            spot.AppendChild(batteryStatus);
            spot.AppendChild(geoLocation);
            spot.AppendChild(type);
            root.AppendChild(spot);


            string aenviar = xmlToSend.InnerXml;
            enviarXmlStringToMosquitto(aenviar);
        }
    }
}
