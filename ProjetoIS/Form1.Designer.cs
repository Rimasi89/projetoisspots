﻿namespace ParkDACE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textRichDllInfo = new System.Windows.Forms.RichTextBox();
            this.btnStopFetchDllInfo = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // textRichDllInfo
            // 
            this.textRichDllInfo.Location = new System.Drawing.Point(10, 11);
            this.textRichDllInfo.Name = "textRichDllInfo";
            this.textRichDllInfo.Size = new System.Drawing.Size(389, 214);
            this.textRichDllInfo.TabIndex = 1;
            this.textRichDllInfo.Text = "";
            this.textRichDllInfo.TextChanged += new System.EventHandler(this.textRichDllInfo_TextChanged);
            // 
            // btnStopFetchDllInfo
            // 
            this.btnStopFetchDllInfo.Location = new System.Drawing.Point(10, 230);
            this.btnStopFetchDllInfo.Name = "btnStopFetchDllInfo";
            this.btnStopFetchDllInfo.Size = new System.Drawing.Size(75, 23);
            this.btnStopFetchDllInfo.TabIndex = 2;
            this.btnStopFetchDllInfo.Text = "StopFetchDllInfo";
            this.btnStopFetchDllInfo.UseVisualStyleBackColor = true;
            this.btnStopFetchDllInfo.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(405, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(387, 213);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 266);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnStopFetchDllInfo);
            this.Controls.Add(this.textRichDllInfo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RichTextBox textRichDllInfo;
        private System.Windows.Forms.Button btnStopFetchDllInfo;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Timer timer1;
    }
}

