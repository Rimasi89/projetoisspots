﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SmartPark
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();


            config.Routes.MapHttpRoute(
              name: "GetSpotByPark",
              routeTemplate: "api/parkingspot/getspotsbyparkname/{parkname}",
              defaults: new { controller = "parkingspot", action = "GetSpotsByParkname" }
            );
            config.Routes.MapHttpRoute(
                name: "GetSpots",
                routeTemplate: "api/parkingspot/getspots",
                defaults: new { controller = "parkingspot", action = "GetSpots"  }
            );
            config.Routes.MapHttpRoute(
                name: "GetSpot",
                routeTemplate: "api/parkingspot/getspot/{id}",
                defaults: new { controller = "parkingspot", action = "GetSpot" }
            );
            config.Routes.MapHttpRoute(
                name: "GetParkingSpotSensors",
                routeTemplate: "api/parkingspot/getparkingspotsensors",
                defaults: new { controller = "parkingspot", action = "GetParkingSpotSensors" }
            );
            config.Routes.MapHttpRoute(
              name: "GetParkingSpotSensorsByParkname",
              routeTemplate: "api/parkingspot/getparkingspotsensorsbyparkname/{parkname}",
              defaults: new { controller = "parkingspot", action = "GetParkingSpotSensorsByParkname" }
            );
            config.Routes.MapHttpRoute(
              name: "GetOccupancyRateByParkname",
              routeTemplate: "api/parkingspot/getoccupancyratebyparkname/{parkname}",
              defaults: new { controller = "parkingspot", action = "GetOccupancyRateByParkname" }
            );
            config.Routes.MapHttpRoute(
              name: "GetParkingSpotInTime",
              routeTemplate: "api/parkingspot/getparkingspotintime",
              defaults: new { controller = "parkingspot", action = "GetParkingSpotInTime" }
            );
            config.Routes.MapHttpRoute(
              name: "GetParkingSpotByParknameInTime",
              routeTemplate: "api/parkingspot/getparkingspotbyparknameintime/{parkname}",
              defaults: new { controller = "parkingspot", action = "GetParkingSpotByParknameInTime" }
            );
            config.Routes.MapHttpRoute(
             name: "GetParkingSpotByParknameBetweenTime",
             routeTemplate: "api/parkingspot/getparkingspotbyparknamebetweentime/{parkname}",
             defaults: new { controller = "parkingspot", action = "GetParkingSpotByParknameBetweenTime" }
           );
        }
    }
}
