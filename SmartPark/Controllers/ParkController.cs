﻿using SmartPark.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SmartPark.Controllers
{
    public class ParkController : ApiController
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SmartPark.Properties.Settings.ConnStr"].ConnectionString;
        public IEnumerable<Park> GetParks()
        {

            List<Park> parks = new List<Park>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM Park", conn);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Park park = new Park
                    {
                        Id = (int)reader["id"],
                        Name = (reader["name"] == DBNull.Value) ? "" : (string)reader["name"],
                        NumberSpots = (reader["numberSpots"] == DBNull.Value) ? 0 : (int)reader["numberSpots"],
                        Description = (reader["numberSpots"] == DBNull.Value) ? "" : (string)reader["description"],
                        OperatingHours = (reader["operatingHours"] == DBNull.Value) ? "" : (string)(reader["operatingHours"]),
                        NumberOfSpecialSpots = (reader["numberSpots"] == DBNull.Value) ? 0 : (int)reader["numberSpots"],
                    };
                    parks.Add(park);
                }
                reader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return parks;
        }

    }
}