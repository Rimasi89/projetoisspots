﻿using SmartPark.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
namespace SmartPark.Controllers
{
    public class ParkingSpotController : ApiController
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SmartPark.Properties.Settings.ConnStr"].ConnectionString;

        public IHttpActionResult GetSpots()
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM ParkingSpot", conn);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ParkingSpot park = new ParkingSpot
                    {
                        Id = (int)reader["id"],
                        Type = (reader["type"] == DBNull.Value) ? "" : (string)reader["type"],
                        Name = (reader["name"] == DBNull.Value) ? "" : (string)reader["name"],
                        Location = (reader["location"] == DBNull.Value) ? "" : (string)reader["location"],
                        SlotStatus = (bool)reader["slotStatus"],
                        BatteryStatus = (bool)(reader["batteryStatus"]),
                        Timestamp = (DateTime)reader["timestamp"],
                        IdPark = (int)reader["idPark"],
                    };
                    parkingSpots.Add(park);
                }
                reader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(parkingSpots);
        }
        public IHttpActionResult GetSpotsByParkname(String parkname)
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT id FROM Park WHERE name=@name", conn);
                cmd.Parameters.Add(new SqlParameter("@name", parkname));
                SqlDataReader reader = cmd.ExecuteReader();

                Park park = new Park();
                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        park.Id = (int)reader["id"];
                    }
                }
                reader.Close();
                if (park != null)
                {
                    SqlCommand parkingCommand = new SqlCommand("SELECT * FROM ParkingSpot WHERE idPark = @idPark", conn);
                    parkingCommand.Parameters.Add(new SqlParameter("@idPark", park.Id));
                    SqlDataReader parkingReader = parkingCommand.ExecuteReader();
                    while (parkingReader.Read())
                    {
                        ParkingSpot parkingSpot = new ParkingSpot
                        {
                            Id = (int)parkingReader["id"],
                            Type = (parkingReader["type"] == DBNull.Value) ? "" : (string)parkingReader["type"],
                            Name = (parkingReader["name"] == DBNull.Value) ? "" : (string)parkingReader["name"],
                            Location = (parkingReader["location"] == DBNull.Value) ? "" : (string)parkingReader["location"],
                            SlotStatus = (bool)parkingReader["slotStatus"],
                            BatteryStatus = (bool)(parkingReader["batteryStatus"]),
                            Timestamp = (DateTime)parkingReader["timestamp"],
                            IdPark = (int)parkingReader["idPark"],
                        };
                        parkingSpots.Add(parkingSpot);
                    }
                    parkingReader.Close();
                }


                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(parkingSpots);
        }
        public IHttpActionResult GetSpot(String id)
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM ParkingSpot WHERE NAME = @name", conn);
                cmd.Parameters.Add(new SqlParameter("@name", id));
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ParkingSpot park = new ParkingSpot
                    {
                        Id = (int)reader["id"],
                        Type = (reader["type"] == DBNull.Value) ? "" : (string)reader["type"],
                        Name = (reader["name"] == DBNull.Value) ? "" : (string)reader["name"],
                        Location = (reader["location"] == DBNull.Value) ? "" : (string)reader["location"],
                        SlotStatus = (bool)reader["slotStatus"],
                        BatteryStatus = (bool)(reader["batteryStatus"]),
                        Timestamp = (DateTime)reader["timestamp"],
                        IdPark = (int)reader["idPark"],
                    };
                    parkingSpots.Add(park);
                }
                reader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(parkingSpots);
        }
        public IHttpActionResult GetParkingSpotSensors()
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT * FROM ParkingSpot WHERE batteryStatus = @batteryStatus", conn);
                cmd.Parameters.Add(new SqlParameter("@batteryStatus",false));
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ParkingSpot park = new ParkingSpot
                    {
                        Id = (int)reader["id"],
                        Type = (reader["type"] == DBNull.Value) ? "" : (string)reader["type"],
                        Name = (reader["name"] == DBNull.Value) ? "" : (string)reader["name"],
                        Location = (reader["location"] == DBNull.Value) ? "" : (string)reader["location"],
                        SlotStatus = (bool)reader["slotStatus"],
                        BatteryStatus = (bool)(reader["batteryStatus"]),
                        Timestamp = (DateTime)reader["timestamp"],
                        IdPark = (int)reader["idPark"],
                    };
                    parkingSpots.Add(park);
                }
                reader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(parkingSpots);
        }
        public IHttpActionResult GetParkingSpotSensorsByParkname(String parkname)
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT id FROM Park WHERE name=@name", conn);
                cmd.Parameters.Add(new SqlParameter("@name", parkname));
                SqlDataReader reader = cmd.ExecuteReader();

                Park park = new Park();
                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        park.Id = (int)reader["id"];
                    }
                }
                reader.Close();
                if (park != null)
                {
                    SqlCommand parkingCommand = new SqlCommand("SELECT * FROM ParkingSpot WHERE idPark = @idPark AND batteryStatus = @batteryStatus", conn);
                    parkingCommand.Parameters.Add(new SqlParameter("@idPark", park.Id));
                    parkingCommand.Parameters.Add(new SqlParameter("@batteryStatus", false));
                    SqlDataReader parkingReader = parkingCommand.ExecuteReader();
                    while (parkingReader.Read())
                    {
                        ParkingSpot parkingSpot = new ParkingSpot
                        {
                            Id = (int)parkingReader["id"],
                            Type = (parkingReader["type"] == DBNull.Value) ? "" : (string)parkingReader["type"],
                            Name = (parkingReader["name"] == DBNull.Value) ? "" : (string)parkingReader["name"],
                            Location = (parkingReader["location"] == DBNull.Value) ? "" : (string)parkingReader["location"],
                            SlotStatus = (bool)parkingReader["slotStatus"],
                            BatteryStatus = (bool)(parkingReader["batteryStatus"]),
                            Timestamp = (DateTime)parkingReader["timestamp"],
                            IdPark = (int)parkingReader["idPark"],
                        };
                        parkingSpots.Add(parkingSpot);
                    }
                    parkingReader.Close();
                }


                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(parkingSpots);
        }
        public IHttpActionResult GetOccupancyRateByParkname(String parkname)
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            int total = 0;
            int occupied = 0;
            double occupancy = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT id FROM Park WHERE name=@name", conn);
                cmd.Parameters.Add(new SqlParameter("@name", parkname));
                SqlDataReader reader = cmd.ExecuteReader();

                Park park = new Park();
                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        park.Id = (int)reader["id"];
                    }
                }
                reader.Close();
                if (park != null)
                {
                    SqlCommand parkingCommand = new SqlCommand("SELECT * FROM ParkingSpot WHERE idPark = @idPark", conn);
                    parkingCommand.Parameters.Add(new SqlParameter("@idPark", park.Id));
                    SqlDataReader parkingReader = parkingCommand.ExecuteReader();
                    while (parkingReader.Read())
                    {
                        total++;
                    }
                    parkingReader.Close();


                    SqlCommand parkingCommandFree = new SqlCommand("SELECT * FROM ParkingSpot WHERE idPark = @idPark AND slotStatus = @slotStatus", conn);
                    parkingCommandFree.Parameters.Add(new SqlParameter("@idPark", park.Id));
                    parkingCommandFree.Parameters.Add(new SqlParameter("@slotStatus", false));
                    SqlDataReader parkingReaderFree = parkingCommand.ExecuteReader();
                    while (parkingReaderFree.Read())
                    {


                        if((bool)parkingReaderFree["slotStatus"])
                        {
                            occupied++;
                        }
                    }
                    parkingReaderFree.Close();
                }

                if(total > 0)
                {
                    occupancy = Math.Round(((double)occupied / (double)total),2) * 100;
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(occupancy);
        }
        [HttpPost]
        public IHttpActionResult GetParkingSpotInTime([FromBody] String time)
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();


                    SqlCommand parkingCommand = new SqlCommand("SELECT * FROM ParkingSpot WHERE timestamp = CONVERT(datetime,@timestamp,104)", conn);
                    parkingCommand.Parameters.Add(new SqlParameter("@timestamp", time));
                    SqlDataReader parkingReader = parkingCommand.ExecuteReader();
                    while (parkingReader.Read())
                    {
                            ParkingSpot parkingSpot = new ParkingSpot
                            {
                                Id = (int)parkingReader["id"],
                                Type = (parkingReader["type"] == DBNull.Value) ? "" : (string)parkingReader["type"],
                                Name = (parkingReader["name"] == DBNull.Value) ? "" : (string)parkingReader["name"],
                                Location = (parkingReader["location"] == DBNull.Value) ? "" : (string)parkingReader["location"],
                                SlotStatus = (bool)parkingReader["slotStatus"],
                                BatteryStatus = (bool)(parkingReader["batteryStatus"]),
                                Timestamp = (DateTime)parkingReader["timestamp"],
                                IdPark = (int)parkingReader["idPark"],
                            };
                            parkingSpots.Add(parkingSpot);
                    }
                parkingReader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(parkingSpots);
        }
        [HttpPost]
        public IHttpActionResult GetParkingSpotByParknameInTime(String parkname , [FromBody] String time)
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();

                System.Diagnostics.Debug.WriteLine("Json " + parkname);
                SqlCommand cmd = new SqlCommand("SELECT id FROM Park WHERE name=@name", conn);
                cmd.Parameters.Add(new SqlParameter("@name", parkname));
                SqlDataReader reader = cmd.ExecuteReader();

                Park park = new Park();
                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        park.Id = (int)reader["id"];
                    }
                }
                reader.Close();
                if (park != null)
                {
                    SqlCommand parkingCommand = new SqlCommand("SELECT * FROM ParkingSpot WHERE idPark = @idPark AND timestamp = CONVERT(datetime,@timestamp,104)", conn);
                    parkingCommand.Parameters.Add(new SqlParameter("@idPark", park.Id));
                    parkingCommand.Parameters.Add(new SqlParameter("@timestamp", time));
                    SqlDataReader parkingReader = parkingCommand.ExecuteReader();
                    while (parkingReader.Read())
                    {
                        if (!(bool)parkingReader["slotStatus"])
                        {
                            ParkingSpot parkingSpot = new ParkingSpot
                            {
                                Id = (int)parkingReader["id"],
                                Type = (parkingReader["type"] == DBNull.Value) ? "" : (string)parkingReader["type"],
                                Name = (parkingReader["name"] == DBNull.Value) ? "" : (string)parkingReader["name"],
                                Location = (parkingReader["location"] == DBNull.Value) ? "" : (string)parkingReader["location"],
                                SlotStatus = (bool)parkingReader["slotStatus"],
                                BatteryStatus = (bool)(parkingReader["batteryStatus"]),
                                Timestamp = (DateTime)parkingReader["timestamp"],
                                IdPark = (int)parkingReader["idPark"],
                            };
                            parkingSpots.Add(parkingSpot);
                        }
                    parkingReader.Close();
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(parkingSpots);
        }
        [HttpPost]
        public IHttpActionResult GetParkingSpotByParknameBetweenTime(String parkname, [FromBody] DateInterval date)
        {
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                conn.Open();

                System.Diagnostics.Debug.WriteLine("Json " + parkname);
                SqlCommand cmd = new SqlCommand("SELECT id FROM Park WHERE name=@name", conn);
                cmd.Parameters.Add(new SqlParameter("@name", parkname));
                SqlDataReader reader = cmd.ExecuteReader();

                Park park = new Park();
                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        park.Id = (int)reader["id"];
                    }
                }
                reader.Close();
                if (park != null)
                {
                    SqlCommand parkingCommand = new SqlCommand("SELECT * FROM ParkingSpot WHERE idPark = @idPark AND timestamp BETWEEN CONVERT(datetime,@inicialDate,104) AND CONVERT(datetime,@finalDate,104)", conn);
                    parkingCommand.Parameters.Add(new SqlParameter("@idPark", park.Id));
                    parkingCommand.Parameters.Add(new SqlParameter("@inicialDate", date.InicialDate));
                    parkingCommand.Parameters.Add(new SqlParameter("@finalDate", date.FinalDate));
                    SqlDataReader parkingReader = parkingCommand.ExecuteReader();
                    while (parkingReader.Read())
                    {

                            ParkingSpot parkingSpot = new ParkingSpot
                            {
                                Id = (int)parkingReader["id"],
                                Type = (parkingReader["type"] == DBNull.Value) ? "" : (string)parkingReader["type"],
                                Name = (parkingReader["name"] == DBNull.Value) ? "" : (string)parkingReader["name"],
                                Location = (parkingReader["location"] == DBNull.Value) ? "" : (string)parkingReader["location"],
                                SlotStatus = (bool)parkingReader["slBotStatus"],
                                BatteryStatus = (bool)(parkingReader["batteryStatus"]),
                                Timestamp = (DateTime)parkingReader["timestamp"],
                                IdPark = (int)parkingReader["idPark"],
                            };
                            parkingSpots.Add(parkingSpot);
                    }
                    parkingReader.Close();

                }
                conn.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error " + ex.ToString());

                if (conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return Ok(parkingSpots);
        }


    }

}