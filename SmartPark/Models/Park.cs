﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartPark.Models
{
    public class Park
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberSpots { get; set; }
        public string Description { get; set; }
        public string OperatingHours { get; set; }
        public int NumberOfSpecialSpots { get; set; }
    }
}