﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartPark.Models
{
    public class ParkingSpot
    {

        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public bool SlotStatus { get; set; }
        public DateTime Timestamp { get; set; }
        public bool BatteryStatus { get; set; }
        public int IdPark { get; set; }
    }
}