﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Data.SqlClient;
using System.Xml;

namespace ParkSS
{
    public partial class Form1 : Form
    {
        MqttClient client = null;
        string[] topics = { "spot" };
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ParkSS.Properties.Settings.ConnStr"].ConnectionString;
        SqlConnection conn = null;
        XmlDocument dadosLidos = null;
        public Form1()
        {
            conn = new SqlConnection(connectionString);
            client = new MqttClient("127.0.0.1");
            InitializeComponent();
            client.Connect(Guid.NewGuid().ToString());
            if (!client.IsConnected)
            {
                richTextBox1.AppendText("Error connecting to broker\n");
            }
            else
            {
                richTextBox1.AppendText("Connected to broker\n");
            }
            client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;

            byte[] qosLevels = { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE };
            client.Subscribe(topics, qosLevels);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

           
           
            client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;

            byte[] qosLevels = { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE};
            client.Subscribe(topics, qosLevels);
        }

        private void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            dadosLidos = new XmlDocument();
            string spot = Encoding.UTF8.GetString(e.Message);
            this.BeginInvoke((MethodInvoker)delegate
            {
                //acesso a componentes visuais do form devem ser colocados aqui, senão há problemas com as threads
                richTextBox1.AppendText($"{e.Topic} : {spot}\n\n");

            });
            dadosLidos.LoadXml(spot);
            conn.Close();
            
            int idPark = getParkId(dadosLidos.SelectSingleNode("/spots").Attributes["parkName"].Value);
            conn.Open();
            SqlCommand cmd = new SqlCommand("UPDATE ParkingSpot SET slotStatus = @slotStatus, timestamp =  CONVERT(datetime,@timestamp,104), batteryStatus=@batteryStatus WHERE name = @name", conn);
            cmd.Parameters.Add(new SqlParameter("@name", dadosLidos.SelectSingleNode("/spots/spot/id").InnerText));
            cmd.Parameters.Add(new SqlParameter("@slotStatus", SqlDbType.Bit));
            cmd.Parameters["@slotStatus"].Value = int.Parse(dadosLidos.SelectSingleNode("/spots/spot/parkingSpotStatus").InnerText);
            cmd.Parameters.Add(new SqlParameter("@timestamp", dadosLidos.SelectSingleNode("/spots/spot/timestamp").InnerText));
            cmd.Parameters.Add(new SqlParameter("@batteryStatus", SqlDbType.Bit));
            cmd.Parameters["@batteryStatus"].Value = int.Parse(dadosLidos.SelectSingleNode("/spots/spot/batteryStatus").InnerText);

            int nRowsAffected = cmd.ExecuteNonQuery();
            if(nRowsAffected != 1)
            {

                SqlCommand cmd2 = new SqlCommand("INSERT INTO ParkingSpot (type,name,location,slotStatus,timestamp,batteryStatus,idPark) " +
                    "VALUES(@type,@name,@location,@slotStatus,CONVERT(datetime,@timestamp,104),@batteryStatus,@idPark)", conn);
                cmd2.Parameters.Add(new SqlParameter("@type", dadosLidos.SelectSingleNode("/spots/spot/type").InnerText));
                cmd2.Parameters.Add(new SqlParameter("@location", dadosLidos.SelectSingleNode("/spots/spot/location").InnerText));
                cmd2.Parameters.Add(new SqlParameter("@idPark", idPark));
                cmd2.Parameters.Add(new SqlParameter("@name", dadosLidos.SelectSingleNode("/spots/spot/id").InnerText));
                cmd2.Parameters.Add(new SqlParameter("@slotStatus", SqlDbType.Bit));
                cmd2.Parameters["@slotStatus"].Value = int.Parse(dadosLidos.SelectSingleNode("/spots/spot/parkingSpotStatus").InnerText);
                cmd2.Parameters.Add(new SqlParameter("@timestamp", dadosLidos.SelectSingleNode("/spots/spot/timestamp").InnerText));
                cmd2.Parameters.Add(new SqlParameter("@batteryStatus", SqlDbType.Bit));
                cmd2.Parameters["@batteryStatus"].Value = int.Parse(dadosLidos.SelectSingleNode("/spots/spot/batteryStatus").InnerText);
                int nRowsAffected2 = cmd2.ExecuteNonQuery();
            }
            conn.Close();

        }

        private int getParkId(string parkName)
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT id FROM Park WHERE name=@name", conn);
            cmd.Parameters.Add(new SqlParameter("@name", parkName));
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            int parkId = (int)reader["id"];
            conn.Close();
            return parkId;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            
        }
    }
    
}
